var app = angular.module('WnServices',[/**'ngSanitize','ngMaterial','ngRoute','ui.ace','ng-file-input','ngTable','ngAnimate','chart.js','dndLists','ngImgCrop'**/]);

app.controller('MainController', function($scope,$http/**,$http,$mdDialog,NgTableParams,ngTableEventsChannel**/) {
	
	$scope.validar = function() {
        if ($scope.contato.nome == null) return false;
        if ($scope.contato.email == null) return false;
        if ($scope.contato.telefone == null) return false;
        if ($scope.contato.mensagem == null) return false;
        return true;
    }

    $scope.tenteNovamente = function() {
        $scope.erro = false;
    }

    $scope.novaMensagem = function() {
        $scope.enviado = false;
    }


    $scope.enviarContato = function () {
        if ($scope.validar()) {
            $scope.waiting = true;
            $http({
                method: 'POST',
                url: "http://www.etssoft.com.br/gerinusPay/telegram/todatorta",
                data: $scope.contato,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function(response) { 
                console.log(response);
                $scope.contato = {};
                $scope.enviado = true;
            }); 
        }
        else {
            $scope.erro = true;
            console.log('erro');
        }
        
    }

	$scope.init = function() {
		$scope.contato = {
            nome: null,
            email: null,
            telefone: null,
            mensagem: null
        };

        $scope.enviado = false;
        $scope.erro = false;

		var token = '2204081025.71a13b7.32258c82aafa4daa99affd56f415c7f8';
        $http.get('https://api.instagram.com/v1/users/self/media/recent?access_token=' + token).then(function(response) { 
        	$scope.imgs = response.data.data;
        	console.log($scope.imgs);
        	//Essas function's estão no tamplates/js/scripts.js
        	setTimeout(function() {
        		initScript();
        		loadScript();
        	},100);
        	
        });
	}
});